#ifndef CHECK_H
#define	CHECK_H

#include <stdio.h>
#include <string.h>
#include "papi.h"
#include "papiStdEventDefs.h"
#include <omp.h>

#ifdef __cplusplus
extern "C" {
#endif

int checkPAPI();
int checkfp_ins();
int checkfp_ops();
int checkfpu_idl();
int checkbr_mis();
int checkl1_tcm();
int checkl2_tcm();

#ifdef __cplusplus
}
#endif

#endif