#include "function.h"

int numthread;
pthread_t tid[16];
float execution;
int idle;
int operations;
int logicalCPU;

//  ----Funzioni per accedere ai valori dei registri----
void cpuID(unsigned i, unsigned *regs) {
        #ifdef _WIN32
	__cpuid((int *)regs, (int)i);
        #else
	asm volatile("cpuid" : "=a" (regs[0]), "=b" (regs[1]), "=c" (regs[2]), "=d" (regs[3]): "a" (i), "c" (0));
        #endif
}

//  ----Funzioni che utilizzano cpuID----

// Core logici CPU
int cpuLogical(unsigned *regs) {
    cpuID(1, regs);
    int logical = (regs[1] >> 16) & 0xff;
    return logical;
}

// HyperThreading della CPU
unsigned cpuHyperThreading(unsigned *regs, int cores, int logicalCPU) {
    cpuID(1, regs);
    int cpuFeatures = regs[3];
    unsigned hyperThreading = cpuFeatures & (1<<28) && cores < logicalCPU;
    return hyperThreading;
}

// Produttore della CPU
void cpuVendor(unsigned *regs, char *vendor) {
    cpuID(0, regs);
    ((unsigned *)vendor)[0] = regs[1];  //ebx
    ((unsigned *)vendor)[1] = regs[3];  //edx
    ((unsigned *)vendor)[2] = regs[2];  //ecx
}

// Numero di core per CPU
int cpuCores(unsigned *regs, char *vendor) {
    int cores;
    if (!strncmp(vendor,"GenuineIntel",12)) {
        // Ottenere info DCP cache
        cpuID(4, regs);
        cores = ((regs[0] >> 26) & 0x3f) + 1;
    }
    else if (!strncmp(vendor,"AuthenticAMD",12)) {
        // Ottenere NC: (Number of CPU cores - 1)
        cpuID(0x80000008, regs);
        cores = ((unsigned)(regs[2] & 0xff)) + 1;
    }
    return cores;
}

// Nome completo della CPU
void cpuName(unsigned *regs, char *name) {
    cpuID(0x80000002, regs);
    int i=0;
    for(i=0;i<4;i++) {
	((unsigned *)name)[i] = regs[i];
    }
    cpuID(0x80000003, regs);
    for(i=4;i<8;i++) {
	((unsigned *)name)[i] = regs[i-4];
    }
    cpuID(0x80000004, regs);
    for(i=8;i<12;i++) {
	((unsigned *)name)[i] = regs[i-8];
    }
}


//  ----Funzione che crea i thread per il conteggio delle istruzioni floating point----

void floatins(int logicalCPU, float execution, int idle, int operations) {
	for(numthread=0;numthread<logicalCPU;numthread++) {
	  if(pthread_create(&(tid[numthread]), NULL, &fpins_thread,(void*)numthread)<0)
		{
			printf("ERROR in creating thread %d",numthread);
		}
	}
	int ret;
	for(numthread=0;numthread<logicalCPU;numthread++)
	{
		ret=pthread_join(tid[numthread], NULL);
		if (ret != 0)
			{
			  printf ("\nJoin failed \n");
 			}
		else printf("\nThread  %d Over\n",numthread);
	 }
}

//  ----Thread per il conteggio di istruzioni Floating point----

void* fpins_thread(void* argc) {
	int id=(int)argc;
	int percentuale=10;
	int cicli=0,cicli1=0,opcic=0,optot=0;
	float a=1.1, b=3.89, c=27.11;
	int event[NUM_EVENTS]={PAPI_FP_INS};
	long long values[NUM_EVENTS];
        float start,end,time=0.0,time1=0.0;
	float dur=(execution/logicalCPU)*1000000;
	float dur1=(execution)/1000.0;
	if(dur1<0) {
		dur1=1;
	}
	int ciclop=operations/1000;
	if(ciclop<1) {
		ciclop=1;
	}
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(id,&cpuset);
	pthread_setaffinity_np(pthread_self(),sizeof(cpu_set_t),&cpuset);
	sleep(idle);
	printf("\nThread %d working on cpu  %d.\n",id,sched_getcpu());
	if (PAPI_start_counters(event, NUM_EVENTS )!= PAPI_OK) {
		fprintf(stderr, "PAPI_start_counters - FAILED\n");
		exit(1);
	}
        while(time<dur) {
		while(time1<dur1) {
	          start=(float)PAPI_get_virt_usec();
		  if(optot<operations) {
                                c=a*b;
				opcic++;
		  }
		  end=(float)PAPI_get_virt_usec();
		  time1+=end-start;
		}
		optot=optot+opcic;
		opcic=0;
		time+=time1;
		time1=0;
	}
	if (PAPI_stop_counters(values, NUM_EVENTS) != PAPI_OK) {
		fprintf(stderr, "PAPI_stopped_counters - FAILED\n");
		exit(1);
	}
	if(optot<operations) {
		printf("Time is not enough for the number of instructions requested.\n");
		printf("Executed ops: %d\n",optot);
		printf("PAPI FP INS: %lld  in %.2f seconds\n\n", values[0],(time/1000000)*2);
	}
	else {
		printf("\nExecuted ops: %d\n",optot);
		printf("PAPI FP INS: %lld  in %.2f seconds\n", values[0],(time/1000000)*2);
		printf("%lld of these are operations inside the code.\n", values[0],values[0]-optot);
	}
	sleep(idle);
	pthread_exit(0);
}

//  ----Funzione che crea i thread per il conteggio delle operazioni floating point----

void floatops(int logicalCPU, float execution, int idle, int operations) {
	for(numthread=0;numthread<logicalCPU;numthread++) {
		if(pthread_create(&(tid[numthread]), NULL, &fpops_thread,(void*)numthread)<0) {
			printf("ERROR in creating thread %d",numthread);
		}
	}
	int ret;
	for(numthread=0;numthread<logicalCPU;numthread++) {
		ret=pthread_join(tid[numthread], NULL);
		if (ret != 0) {
		  printf ("\nJoin failed \n");
		}
		else {
		  printf("Thread %d Over\n",numthread);
		}
	}
}

//  ----Thread per il conteggio di operazioni Floating point----

void* fpops_thread(void* argc) {
	int id=(int)argc;
	int cicli=0,cicli1=0,opcic=0,optot=0;
	float a=1.1, b=3.89, c=27.11;
        float dur=(execution/logicalCPU)*1000000;
	int event[NUM_EVENTS]={PAPI_FP_OPS};
	long long values[NUM_EVENTS];
        float start,end,time=0.0,time1=0.0;
	float dur1=dur/1000;
	int ciclop=operations/1000;
	if(ciclop<1) {
		ciclop=1;
	}
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(id,&cpuset);
	pthread_setaffinity_np(pthread_self(),sizeof(cpu_set_t),&cpuset);
	sleep(idle);
	printf("Thread %d working on cpu  %d.\n",id,sched_getcpu());
	if (PAPI_start_counters(event, NUM_EVENTS )!= PAPI_OK) {
		fprintf(stderr, "PAPI_start_counters - FAILED\n");
		exit(1);
	}
	while(time<dur) {
		while(time1<dur1) {
		  start=(float)PAPI_get_virt_usec();
		  if(optot<operations) {
		    c=a*b;
		    opcic++;
		  }
		  end=(float)PAPI_get_virt_usec();
		  time1+=end-start;
		}
		optot=optot+opcic;
		time+=time1;
		opcic=0;
		time1=0;
	}
	if (PAPI_stop_counters(values, NUM_EVENTS) != PAPI_OK) {
		fprintf(stderr, "PAPI_stopped_counters - FAILED\n");
		exit(1);
	}
	if(optot<operations) {
		printf("Time is not enough for the number of instructions requested.\n");
		printf("Executed ops: %d\n",optot);
		printf("PAPI FP OPS: %lld  in %.2f seconds\n\n", values[0],(time/1000000)*2);
	}
	else{
		printf("Executed ops: %d\n",optot);
		printf("PAPI FP OPS: %lld  in %.2f seconds\n", values[0],(time/1000000)*2);
		printf("%lld of these are operations inside the code.\n", values[0],values[0]-optot);
	}
	sleep(idle);
	pthread_exit(0);
}

//  ----Funzione che crea i thread per monitorare il tempo di idle dell'unita' floating point----

void floatidl(int logicalCPU, float execution, int idle) {
	for(numthread=0;numthread<logicalCPU;numthread++) {
		if(pthread_create(&(tid[numthread]), NULL, &fpuidl_thread,(void*)numthread)<0) {
			printf("ERROR in creating thread %d",numthread);
		}
	}
	int ret;
	for(numthread=0;numthread<logicalCPU;numthread++) {
		ret=pthread_join(tid[numthread], NULL);
		if (ret != 0) {
		  printf ("Join failed \n");
		}
	        else {
		  printf("Thread %d Over\n",numthread);
		}	
	}
}

//  ----Thread per i cicli di idle dell'unita' floating point----

void* fpuidl_thread(void* argc) {
	int id=(int)argc;
	int cicli=0,cicli1=0,opcic=0,optot=0,dur=execution*50000000;
	float a=1.1, b=3.89, c=27.11;
	int event[NUM_EVENTS]={PAPI_FPU_IDL};
	long long values[NUM_EVENTS];
	int dur1=dur/1000;
	int ciclop=operations/1000;
	if(ciclop<1) {
		ciclop=1;
	}
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(id,&cpuset);
	pthread_setaffinity_np(pthread_self(),sizeof(cpu_set_t),&cpuset);
	sleep(idle);
	printf("\nThread %d working on cpu  %d.\n",id,sched_getcpu());
	if (PAPI_start_counters(event, NUM_EVENTS )!= PAPI_OK) {
	  fprintf(stderr, "PAPI_start_counters - FAILED\n");
	  exit(1);
	}
	while(cicli<dur) {
		while(cicli1<dur1) {
			c=a*b;
			if(optot<operations) {
				opcic++;
			}
			cicli1++;
		}
		optot=optot+opcic;
		opcic=0;
		cicli+=cicli1;
		cicli1=0;
	}
	if (PAPI_stop_counters(values, NUM_EVENTS) != PAPI_OK) {
		fprintf(stderr, "PAPI_stopped_counters - FAILED\n");
		exit(1);
	}
	if(optot<operations) {
		printf("\nTime is not enough for the number of instructions requested.\n");
		printf("\nExecuted cycles: %d .Executed ops: %d\n",cicli,optot);
		printf("PAPI FP INS: %lld  in %.2f seconds\n\n", values[0],execution);
	}
	else {
		printf("\nExecuted cycles: %d .Executed ops: %d\n",cicli,optot);
		printf("PAPI FPU IDL: %lld  in %.2f seconds\n", values[0],execution);
		printf("%lld of these are operations inside the code.\n\n", values[0],values[0]-optot);
	}
	sleep(idle);
	pthread_exit(0);
}

//  ----Funzione che crea i thread per il conteggio dei branch mispredictions----

void branchbench(int logicalCPU, float execution, int idle) {
	for(numthread=0;numthread<logicalCPU;numthread++) {
		if(pthread_create(&(tid[numthread]), NULL, &branchthread,(void*)numthread)<0) {
			printf("ERROR in creating thread %d",numthread);
		}
	}
	int ret;
	for(numthread=0;numthread<logicalCPU;numthread++) {
		ret=pthread_join(tid[numthread], NULL);
		if (ret != 0) {
		  printf ("Join failed \n");
		}
	        else {
		  printf("Thread %d Over\n",numthread);
		}
	 }
}

//  ----Thread lavoratore per i conteggi dei branch mispredictions----

void* branchthread(void* argc) {
	int id=(int)argc;
	int numeri[1000000];
	int val,k,m,t,j,min,i,lenght;;
	float start=0.0,end=0.0,tempo=0.0,dur=execution*1000000;
	val=k=2;
	int event[NUM_EVENTS]={PAPI_BR_MSP};
	long long values[NUM_EVENTS];
	long long bran;
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(id,&cpuset);
	pthread_setaffinity_np(pthread_self(),sizeof(cpu_set_t),&cpuset);
	sleep(idle);
	printf("Thread %d working on cpu %d.\n",id,sched_getcpu());
	printf("Inizio Ciclo con conteggi di %d secondi.\n",(int)execution);
	while(tempo<dur) {
		for(m=0;m<k;m++) {
		  numeri[m]= val;			//inizializzazione array
		  val--;
		}
		if (PAPI_start_counters(event, NUM_EVENTS != PAPI_OK)) {
		  fprintf(stderr, "PAPI_start_counters - FAILED\n");
		  exit(1);
		}
		start=(float)PAPI_get_virt_usec();
		for (i=0;i<k;i++) {
		  min = i;
		  for (j=i+1;j<k;j++) {
		    if (numeri[j] < numeri[min])
		      min = j;
		    t = numeri[min];
		    numeri[min] = numeri[i];
		    numeri[i] = t;
		  }
		}
		end=(float)PAPI_get_virt_usec();
		if (PAPI_stop_counters(values, NUM_EVENTS) != PAPI_OK) {
		  fprintf(stderr, "PAPI_stopped_counters - FAILED\n");
		  exit(1);
		}
		printf("Branch mispredictions (thread %d) for a %d elements array: %lld\n",id,k,values[0]);
		tempo+=end-start;
		k=k*2;
		bran+=values[0];
		sleep(idle);
	}
	printf("Total branch mispredictions (thread %d) : %lld\n",id,bran);
	sleep(idle);
	pthread_exit(0);
}

//  ----Funzione che crea i thread per il conteggio dei cache misses totali L1----

void l1tcm(int logicalCPU, float execution, int idle) {
	for(numthread=0;numthread<logicalCPU;numthread++) {
	  if(pthread_create(&(tid[numthread]), NULL, &l1tcm_thread,(void*)numthread)<0) {
	    printf("ERROR in creating thread %d",numthread);
	  }
	}
	int ret;
	for(numthread=0;numthread<logicalCPU;numthread++) {
		ret=pthread_join(tid[numthread], NULL);
		if (ret!=0) {
		  printf ("\nJoin failed \n");
		}
	        else printf("Thread %d Over \n",numthread);
	}
}

//  ----Thread lavoratore che monitora i cache misses L1----

void* l1tcm_thread(void* argc) {
	int id=(int)argc;
	int riga=10,colonna=10,i,j,val=0,a;
	int numeri[1000][1000];
	float durataThread = execution/logicalCPU;
	float start=0.0,end=0.0,tempo=0.0,dur=durataThread*1000;
	int event[NUM_EVENTS]={PAPI_L1_TCM};
	long long values[NUM_EVENTS];
	long long bran=0;
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(id,&cpuset);
	pthread_setaffinity_np(pthread_self(),sizeof(cpu_set_t),&cpuset);
	sleep(idle);
	printf("Thread %d will work on cpu %d for %.2f s.\n",id,sched_getcpu(),durataThread);
	while(tempo<dur) {
		start=(float)PAPI_get_virt_usec();
		for(i=0;i<riga;i++) {
		  for(j=0;j<colonna;j++) {
		    numeri[i][j]= val;	//inizializzazione array
		    val++;
		  }
		}
		if (PAPI_start_counters(event, NUM_EVENTS != PAPI_OK)) {
			fprintf(stderr, "PAPI_start_counters - FAILED\n");
			exit(1);
		}
		for (i=0;i<riga;i++) {
		  for (j= 0;j<colonna;j++) {
		    a=numeri[i][j];
		  }
		}
		for (j=0;i<colonna;j++) {
		  for (i= 0;i<riga;i++) {
		    a=numeri[i][j];
		  }
		}
		if (PAPI_stop_counters(values, NUM_EVENTS) != PAPI_OK) {
			fprintf(stderr, "PAPI_stopped_counters - FAILED\n");
			exit(1);
		}
		bran+=values[0];
		end=(float)PAPI_get_virt_usec();
		tempo+=end-start;
		printf("L1 total cache misses (thread %d) for a matrix [%d][%d]: %lld\n",id,riga,colonna,values[0]);
		if((riga>900)||(colonna>900)) {
		  riga=10;
		  colonna=10;
		}
		else {
		  riga=riga+10;
		  colonna=colonna+10;
		}		
		sleep(idle);
	}
	sleep(idle);
	printf("Thread %d L1 total cache misses: %lld in %.2f s\n",id,bran,durataThread);
	pthread_exit(0);
}

//  ----Funzione che crea i thread per il conteggio delle cache misses totali L2----

void l2tcm(int logicalCPU, float execution, int idle) {
	for(numthread=0;numthread<logicalCPU;numthread++) {
		if(pthread_create(&(tid[numthread]), NULL, &l2tcm_thread,(void*)numthread)<0) {
		  printf("ERROR in creating thread %d",numthread);
		}
	}
	int ret;
	for(numthread=0;numthread<logicalCPU;numthread++) {
		ret=pthread_join(tid[numthread], NULL);
		if (ret != 0) {
		  printf ("\nJoin failed \n");
		}
	        else printf("Thread %d Over \n",numthread);
	}
}

//  ----Thread lavoratore che monitora i cache misses L2----

void* l2tcm_thread(void* argc) {
	int id=(int)argc;
	int riga=10,colonna=10,i,j,val=0,a;
	int numeri[1000][1000];
	float durataThread = execution/logicalCPU;
	float start=0.0,end=0.0,tempo=0.0,dur=durataThread*1000;
	int event[NUM_EVENTS]={PAPI_L2_TCM};
	long long values[NUM_EVENTS];
	long long bran=0;
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(id,&cpuset);
	pthread_setaffinity_np(pthread_self(),sizeof(cpu_set_t),&cpuset);
	sleep(idle);
	printf("Thread %d will work on cpu %d for %.2f s.\n",id,sched_getcpu(),durataThread);
	while(tempo<dur) {
		start=(float)PAPI_get_virt_usec();
		for(i=0;i<riga;i++) {
		  for(j=0;j<colonna;j++) {
		    numeri[i][j]= val;	//inizializzazione array
		    val++;
		  }
		}
		if (PAPI_start_counters(event, NUM_EVENTS != PAPI_OK)) {
		  fprintf(stderr, "PAPI_start_counters - FAILED\n");
		  exit(1);
		}
		for (i=0;i<riga;i++) {
		  for (j= 0;j<colonna;j++) {
		    a=numeri[i][j];
		  }
		}
		for (j=0;i<colonna;j++) {
		  for (i= 0;i<riga;i++) {
		    a=numeri[i][j];
		  }
		}
		if (PAPI_stop_counters(values, NUM_EVENTS) != PAPI_OK) {
		  fprintf(stderr, "PAPI_stopped_counters - FAILED\n");
		  exit(1);
		}
		bran+=values[0];
		end=(float)PAPI_get_virt_usec();
		tempo+=end-start;
		printf("L2 total cache misses (thread %d) for a matrix [%d][%d]: %lld\n",id,riga,colonna,values[0]);
		if((riga>900)||(colonna>900)) {
		  riga=10;
		  colonna=10;
		}
		else {
		  riga=riga+10;
		  colonna=colonna+10;
		}
		sleep(idle);
	}
	sleep(idle);
	printf("Thread %d L2 total cache misses: %lld in %.2f s\n",id,bran,durataThread);
	pthread_exit(0);
}

// ----Funzione che crea i thread per l'esecuzione di Rijndael.----

void rijnd(int logicalCPU) {
	for(numthread=0;numthread<logicalCPU;numthread++) {
		if(pthread_create(&(tid[numthread]), NULL, threadrijnd,(void*)numthread)<0) {
		  printf("ERROR in creating thread %d",numthread);
		}
	}
	int ret;
	for(numthread=0;numthread<logicalCPU;numthread++) {
		ret=pthread_join(tid[numthread], NULL);
		if (ret != 0) {
		  printf ("\nJoin failed \n");
		}
		else {
		  printf("\nThread %d Over\n",numthread);
		}
	}
}

//  ---- Thread che monitora l'esecuzione di Rijndael.----

void* threadrijnd(void* argc) {
	      int id=(int)argc;
	      cpu_set_t cpuset;
	      CPU_ZERO(&cpuset);
	      CPU_SET(id,&cpuset);
	      pthread_setaffinity_np(pthread_self(),sizeof(cpu_set_t),&cpuset);
	      sleep(2);
	      printf("\nThread %d working on cpu  %d.\n",id,sched_getcpu());
	      int event[NUM_EVENTS] = {PAPI_FP_INS};
	      long long values[NUM_EVENTS];
	      if (PAPI_start_counters(event,NUM_EVENTS != PAPI_OK)) {
		fprintf(stderr, "PAPI_start_counters - FAILED\n");
		exit(1);
	      }
	      sleep(2);
	      // Floating point instructions su Rijndael
	      printf("\n\nFloating Point Instructions Rijndael starting..\n\n");
	      makeFIPSTestVectors("fips-test-vectors.txt");
	      makeKATs("ecb_vk.txt", "ecb_vt.txt", "ecb_tbl.txt", "ecb_iv.txt");
	      makeMCTs("ecb_e_m.txt", "ecb_d_m.txt", "cbc_e_m.txt", "cbc_d_m.txt");
	      sleep(2);
	      if (PAPI_stop_counters(values, NUM_EVENTS) != PAPI_OK) {
		fprintf(stderr, "PAPI_stopped_counters - FAILED\n");
		exit(1);
	      }
	      int event1[NUM_EVENTS] = {PAPI_L1_TCM};
	      long long values1[NUM_EVENTS];
	      if (PAPI_start_counters(event1,NUM_EVENTS != PAPI_OK)) {
		fprintf(stderr, "PAPI_start_counters - FAILED\n");
		exit(1);
	      }
	      sleep(2);
	      // L1 Cache misses su Rijndael
	      printf("\n\nTotal L1 cache misses Rijndael starting..\n\n");
	      makeFIPSTestVectors("fips-test-vectors.txt");
	      makeKATs("ecb_vk.txt", "ecb_vt.txt", "ecb_tbl.txt", "ecb_iv.txt");
	      makeMCTs("ecb_e_m.txt", "ecb_d_m.txt", "cbc_e_m.txt", "cbc_d_m.txt");
	      sleep(2);
	      if (PAPI_stop_counters(values1, NUM_EVENTS) != PAPI_OK) {
		fprintf(stderr, "PAPI_stopped_counters - FAILED\n");
		exit(1);
	      }
	      int event2[NUM_EVENTS] = {PAPI_L1_TCM};
	      long long values2[NUM_EVENTS];
	      if (PAPI_start_counters(event2,NUM_EVENTS != PAPI_OK)) {
		fprintf(stderr, "PAPI_start_counters - FAILED\n");
		exit(1);
	      }
	      sleep(2);
	      // L2 Cache misses su Rijndael
	      printf("\n\nTotal L2 cache misses Rijndael starting..\n\n");
	      makeFIPSTestVectors("fips-test-vectors.txt");
	      makeKATs("ecb_vk.txt", "ecb_vt.txt", "ecb_tbl.txt", "ecb_iv.txt");
	      makeMCTs("ecb_e_m.txt", "ecb_d_m.txt", "cbc_e_m.txt", "cbc_d_m.txt");
	      sleep(2);
	      if (PAPI_stop_counters(values2, NUM_EVENTS) != PAPI_OK) {
		fprintf(stderr, "PAPI_stopped_counters - FAILED\n");
		exit(1);
	      }
	      printf("\n\nThread %d :\n\n",id);
	      printf("\nTotal Floating Point Instructions: %lld\n\n", values[0]);
	      printf("\nTotal L1 cache misses: %lld\n\n", values1[0]);
	      printf("\nTotal L2 cache misses: %lld\n\n", values2[0]);
}
