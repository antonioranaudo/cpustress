#ifndef FUNCTION_H
#define	FUNCTION_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <omp.h>
#include <errno.h>
#include <assert.h>
#include <time.h>
#include <string.h>
#include "util.h"
#include "papi.h"
#include "papiStdEventDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

#define NUM_EVENTS 1

int cpuLogical(unsigned *regs);
unsigned cpuHyperThreading (unsigned *regs, int cores, int logicalCPU);
void cpuVendor(unsigned *regs, char *vendor);
int cpuCores(unsigned *regs, char *vendor);
void cpuName(unsigned *regs, char *name);

void floatins(int logicalCPU, float execution, int idle, int operations);
void* fpins_thread(void* argc);
void floatops(int logicalCPU, float execution, int idle, int operations);
void* fpops_thread(void* argc);
void floatidl(int logicalCPU, float execution, int idle);
void* fpuidl_thread(void *argc);

void branchbench(int logicalCPU, float execution, int idle);
void* branchthread(void* argc);

void l1tcm(int logicalCPU, float execution, int idle);
void* l1tcm_thread(void* argc);
void l2tcm(int logicalCPU, float execution, int idle);
void* l2tcm_thread(void* argc);

void rijnd(int logicalCPU);
void* threadrijnd(void* argc);

#ifdef __cplusplus
}
#endif

#endif