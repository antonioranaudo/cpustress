#include "rijndael-api-fst.h"

#ifdef __cplusplus
extern "C" {
#endif

void blockPrint(FILE *fp, const BYTE *block, const char *tag);
void rijndaelVKKAT(FILE *fp, int keyLength);
void rijndaelVTKAT(FILE *fp, int keyLength);
void rijndaelTKAT(FILE *fp, int keyLength, FILE *in);
void makeKATs(const char *vkFile, const char *vtFile, const char *tblFile, const char *ivFile);
void rijndaelECB_MCT(FILE *fp, int keyLength, BYTE direction);
void makeMCTs(const char *ecbEncryptionFile, const char *ecbDecryptionFile, const char *cbcEncryptionFile, const char *cbcDecryptionFile);
void makeFIPSTestVectors(const char *fipsFile);
void rijndaelCBC_MCT(FILE *fp, int keyLength, BYTE direction);
void rijndaelSpeed(int keyBits);

#ifdef __cplusplus
}
#endif