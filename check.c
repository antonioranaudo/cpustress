#include "check.h"

//  ----Funzione CHECK() per vedere se PAPI è installato----

int checkPAPI() {
	int ret;
	int retval = PAPI_library_init(PAPI_VER_CURRENT);
	if (retval != PAPI_VER_CURRENT) {
	  printf("Error in library init.\n");
	  ret=1;
	}
	if (PAPI_set_debug(PAPI_VERB_ECONT) != PAPI_OK) {
	  ret=1;
	}
	retval = PAPI_thread_init((unsigned long (*)(void))(omp_get_thread_num()));
	if(retval != PAPI_OK) {
	  ret=1;
	}
	if (PAPI_num_counters() < 1) {
	  fprintf(stderr, "No hardware counters here, or PAPI not supported.\n");
	  ret=0;
	}
	return ret;
}
//  ----Funzione CHECK() per vedere se PAPI_FP_INS può essere monitorato----
int checkfp_ins() {
	int ret=0;
	int retval;
	PAPI_event_info_t info;

	//Inizializzazione  PAPI
	retval = PAPI_library_init(PAPI_VER_CURRENT);
	if (retval != PAPI_VER_CURRENT) {
	  fprintf(stderr,"PAPI library init error!\n");
	  ret=0;
	}
	//Controllo dell'esistenza del preset PAPI_FP_INS
	retval = PAPI_query_event(PAPI_FP_INS);
	if (retval != PAPI_OK) {
	  fprintf(stderr,"No floating point INSTRUCTIONS counter.\n");
	  ret=0;
	}
	//Info su PAPI_FP_INS
	retval = PAPI_get_event_info( PAPI_FP_INS, &info );
	if ( retval != PAPI_OK ) {
	  fprintf(stderr,"No event info.\n");
	  ret=0;
	}
	if(info.count > 0) {
	  printf("Floating point instruction event is available on this hardware.\n");
	  ret=1;
	}
	if(strcmp( info.derived, "NOT_DERIVED" )) {
	  printf( "This event is a derived event on this hardware.\n" );
	}
	return ret;
}
//  ----Funzione check che controlla se PAPI_FP_OPS è  disponibile----
int checkfp_ops() {
	int ret=0;
	int retval;
	PAPI_event_info_t info;

	//Inizializzazione PAPI
	retval = PAPI_library_init(PAPI_VER_CURRENT);
	if (retval != PAPI_VER_CURRENT) {
	  fprintf(stderr,"PAPI library init error!\n");
	  ret=0;
	}
	//Controllo dell'esistenza del preset PAPI_FP_OPS
	retval = PAPI_query_event(PAPI_FP_OPS );
	if (retval != PAPI_OK) {
	  fprintf(stderr,"No floating point OPERATIONS counter.\n");
	  ret=0;
	}
	//Info su PAPI_FP_OPS
	retval = PAPI_get_event_info( PAPI_FP_OPS, &info );
	if ( retval != PAPI_OK ) {
	  fprintf(stderr,"No event info.\n");
	  ret=0;
	}
	if ( info.count > 0 ) {
	  printf("Floating point operations event is available on this hardware.\n");
	  ret=1;
	}
	if (strcmp( info.derived, "NOT_DERIVED" )) {
	  printf( "This event is a derived event on this hardware.\n" );
	}
	return ret;
}
//  ----Funzione check che controlla se PAPI_FPU_IDL può essere monitorato----
int checkfpu_idl() {
	int ret=0;
	int retval;
	PAPI_event_info_t info;

	//Inizializzazione PAPI
	retval = PAPI_library_init(PAPI_VER_CURRENT);
	if (retval != PAPI_VER_CURRENT) {
	  fprintf(stderr,"PAPI library init error!\n");
	  ret=0;
	}
	//Controllo dell'esistenza del preset PAPI_FPU_IDL
	retval = PAPI_query_event(PAPI_FPU_IDL);
	if (retval != PAPI_OK) {
	  fprintf (stderr,"No floating point unit idle cycle counter.\n");
	  ret=0;
	}
	//Info su PAPI_FPU_IDL
	retval = PAPI_get_event_info( PAPI_FPU_IDL, &info );
	if ( retval != PAPI_OK ) {
	  fprintf (stderr,"No event info.\n");
	  ret=0;
	}
	if ( info.count > 0 ) {
	  printf ("Floating point unit idle cycle event is available on this hardware.\n");
	  ret=1;
	}
	if ( strcmp( info.derived, "NOT_DERIVED" )) {
	  printf ( "This event is a derived event on this hardware.\n" );
	}
	return ret;
}
//  ----Funzione check che controlla se i branch mispredictions possono essere monitorati----
int checkbr_mis() {
	int ret=0;
	int retval;
	PAPI_event_info_t info;

	//Inizializzazione della libreria
	retval = PAPI_library_init(PAPI_VER_CURRENT);
	if (retval != PAPI_VER_CURRENT) {
	  fprintf(stderr,"PAPI library init error!\n");
	  ret=0;
	}
	//Controllo dell'esistenza del preset PAPI_BR_MSP
	retval = PAPI_query_event(PAPI_BR_MSP );
	if (retval != PAPI_OK) {
	  fprintf (stderr,"No branch mispredictions counter.\n");
	  ret=0;
	}
	//Info su PAPI_BR_MSP
	retval = PAPI_get_event_info( PAPI_BR_MSP, &info );
	if ( retval != PAPI_OK ) {
	  fprintf (stderr,"No event info.\n");
	  ret=0;
	}
	if ( info.count > 0 ) {
	  printf ("Branch mispredictions event is available on this hardware.\n");
	  ret=1;
	}
	if (strcmp( info.derived, "NOT_DERIVED" )) {
	  printf ( "This event is a derived event on this hardware.\n" );
	}
	return ret;
}
//  ----Funzione check che controlla se PAPI_L1_TCM è  disponibile----
int checkl1_tcm() {
	int ret=0;
	int retval;
	PAPI_event_info_t info;

	//Inizializzazione PAPI
	retval = PAPI_library_init(PAPI_VER_CURRENT);
	if (retval != PAPI_VER_CURRENT) {
	  fprintf(stderr,"PAPI library init error!\n");
	  ret=0;
	}
	//Controllo dell'esistenza del preset PAPI_L1_TCM
	retval = PAPI_query_event(PAPI_L1_TCM);
	if (retval != PAPI_OK) {
	  fprintf (stderr,"No L1 cache misses counter.\n");
	  ret=0;
	}
	//Info su PAPI_L1_TCM
	retval = PAPI_get_event_info( PAPI_L1_TCM, &info );
	if ( retval != PAPI_OK ) {
	  fprintf (stderr,"No event info.\n");
	  ret=0;
	}
	if ( info.count > 0 ) {
	  printf ("L1 cache misses event is available on this hardware.\n");
	  ret=1;
	}
	if ( strcmp( info.derived, "NOT_DERIVED" )) {
	  printf ( "This event is a derived event on this hardware.\n" );
	}
	return ret;
}
//  ----Funzione check che controlla se PAPI_L2_TCM è  disponibile----
int checkl2_tcm() {
	int ret=0;
	int retval;
	PAPI_event_info_t info;

	//Inizializzazione PAPI
	retval = PAPI_library_init(PAPI_VER_CURRENT);
	if (retval != PAPI_VER_CURRENT) {
	  fprintf(stderr,"PAPI library init error!\n");
	  ret=0;
	}
	//Controllo dell'esistenza del preset PAPI_L2_TCM
	retval = PAPI_query_event(PAPI_L2_TCM );
	if (retval != PAPI_OK) {
	  fprintf (stderr,"No L2 cache misses counter.\n");
	  ret=0;
	}
	//Info su PAPI_L2_TCM
	retval = PAPI_get_event_info( PAPI_L2_TCM, &info );
	if ( retval != PAPI_OK ) {
	  fprintf (stderr,"No event info.\n");
	  ret=0;
	}
	if ( info.count > 0 ) {
	  printf ("L2 cache misses event is available on this hardware.\n");
	  ret=1;
	}
	if ( strcmp( info.derived, "NOT_DERIVED" )) {
	  printf ( "This event is a derived event on this hardware.\n" );
	}
	return ret;
}
