#include "function.h"
#include "check.h"

//Variabili globali

// unsigned regs[4];
// char vendor[12];
// int features;
// char name[49];
// int cores;
// unsigned hyperThreads;

float execution;
int operations,idle,logicalCPU,radio;

//  ----Funzione main----

int main (int argn, char **cmd) {
	switch(argn) {
	  case 3: {
	    radio=atoi(cmd[1]);
	    logicalCPU=atoi(cmd[2]);
	  }
	  break;
	  case 5: {
	    radio=atoi(cmd[1]);
	    logicalCPU=atoi(cmd[2]);
	    execution = atof(cmd[3]);
	    idle = atoi(cmd[4]);
	  }
	  break;
	  case 6: {
	    radio=atoi(cmd[1]);
	    logicalCPU=atoi(cmd[2]);
	    execution = atof(cmd[3]);
	    idle = atoi(cmd[4]);
	    operations = atoi(cmd[5]);
	  }
	  break;
	  default: exit(1);
	}
	checkPAPI();
	switch(radio) {
	  case 1: floatins(logicalCPU,execution,idle,operations); break;
	  case 2: floatops(logicalCPU,execution,idle,operations); break;
	  case 3: floatidl(logicalCPU,execution,idle); break;
	  case 4: l1tcm(logicalCPU,execution,idle); break;
	  case 5: l2tcm(logicalCPU,execution,idle); break;
	  case 6: branchbench(logicalCPU,execution,idle); break;
	  case 7: rijnd(logicalCPU); break;
	  default: exit(1);
	}
	exit(0);
}

// int main()
// {
// 	int test;
// 	int bol=0;
// 	char line[30];
// 	cpuVendor(regs,vendor);	
// 	printf("Vendor: %s \n",vendor);
// 	cpuName(regs,name);
// 	printf("Name: %s \n",name);
// 	logicalCPU = cpuLogical(regs);
// 	printf("Logical CPUs: %d \n",logicalCPU);
// 	cores = cpuCores(regs, vendor);
// 	printf("CPU cores: %d \n",cores);
// 	hyperThreads = cpuHyperThreading(regs, cores, logicalCPU);
// 	printf("HyperThreading: %s \n",(hyperThreads ? "YES" : "NO"));
// 	
// 	if(checkPAPI()) {
// 	}	
// 	else {
// 	  printf("PAPI not supported!\n");
// 	  exit(0);
// 	}
// 	
// 	while(1) {
// 	  printf("Benchmarks available are:\n");
// 	  printf("1) Arithmetic Benchmarks\n");
// 	  printf("2) Branch mispredictions Benchmark\n");
// 	  printf("3) Memory Benchmark\n");
// 	  printf("4) To run Rijndael Benchmark\n");
// 	  printf("0) To exit.\n");
// 	  printf("Choose one to be executed.\n");
// 	  scanf("%d",&test);
// 	  switch(test){
// 	    case 0: {
// 	      exit(0);
// 	    }
// 		break;
// 	    //Arithmetic benchmarks
// 	    case 1: {
// 			printf("\nCounters available are:\n");
// 			if(checkfp_ins()) {
// 			  printf("INS ---> Floating point instructions\n");
// 			}
// 			if(checkfp_ops()) {
// 			  printf("OPS ---> Floating point operations\n");
// 			}
// 			if(checkfpu_idl()) {
// 			  printf("IDL ---> Cycles floating point units are idle\n");
// 			}
// 			scanf("%s",line);
// 			while((strcmp(line,"INS")!=0)&&(strcmp(line,"OPS")!=0)&&(strcmp(line,"IDL")!=0)) {
// 				printf("\nInput isn't correct.Please insert:\n");
// 				printf("\nINS for Floating point instructions\n");
// 				printf("\nOPS for Floating point operations\n");
// 				printf("\nIDL for cycles floating point units are idle.\n");
// 				scanf("%s",line);
// 			}
// 
// 			//Caso Floating Point Instructions
// 
// 			if(strcmp(line,"INS")==0) {
// 			  if(checkfp_ins()) {
// 			    printf("Insert execution time (in seconds):\n");
// 			    scanf("%f",&execution);
// 			    printf("Insert idle time (in seconds):\n");
// 			    scanf("%d",&idle);
// 			    printf("Insert the number of operation to be executed,if possible:\n");
// 			    scanf("%d",&operations);
// 			    floatins(logicalCPU,execution,idle,operations);
// 			  }
// 			}
// 
// //			Caso Floating Point Operations
// 
// 			if(strcmp(line,"OPS")==0) {
// 			  if(checkfp_ops()) {
// 			    printf("Insert execution time (in seconds):\n");
// 			    scanf("%f",&execution);
// 			    printf("Insert idle time (in seconds):\n");
// 			    scanf("%d",&idle);
// 			    printf("Insert the number of operation to be executed,if possible:\n");
// 			    scanf("%d",&operations);
// 			    floatops(logicalCPU,execution,idle,operations);
// 			  }
// 			}
// 
// //			Caso Floating Point Idle
// 
// 			if(strcmp(line,"IDL")==0) {
// 			  if(checkfpu_idl()) {
// 			  printf("Insert execution time (in seconds):\n");
// 			  scanf("%f",&execution);
// 			  printf("Insert idle time (in seconds):\n");
// 			  scanf("%d",&idle);
// 			  floatidl(logicalCPU,execution,idle);
// 			  }
// 			}
// 		}
// 		break;
// 	    //Branch mispredictions
// 	    case 2: {
// 			if(checkbr_mis()) {
// 			  ("This benchmark sort an array of integers.The array length increases during the execution.\n");
// 			  printf("Insert execution time (in seconds) :\n");
// 			  scanf("%f",&execution);
// 			  printf("Insert idle time (in seconds) :\n");
// 			  scanf("%d",&idle);
// 			  branchbench(logicalCPU,execution,idle);
// 			}
// 			else {
// 			  printf("Branch Mispredictions counter is NOT AVAILABLE\n");
// 			}
// 		}
// 		break;
// 	    //Memory benchmarks
// 	    case 3: {
// 			printf("Memory Counters available are:\n");
// 			if(checkl1_tcm()) {
// 			  printf("Insert L1 for L1 total cache misses\n");
// 			}
// 			if(checkl2_tcm()) {
// 			  printf("Insert L2 for L2 total cache misses\n");
// 			}
// 			printf("Choose one:\n");
// 			scanf("%s",line);
//         
// 			while((strcmp(line,"L1")!=0)&&(strcmp(line,"L2")!=0)) {
// 			  printf("\nInput isn't correct. Please insert:");
// 			  printf("\nL1 for L1 total cache misses");
// 			  printf("\nL2 for L2 total cache misses");
// 			  scanf("%s",line);
// 			}
// 
// //			Caso L1 Total Cache Misses
// 
// 			if(strcmp(line,"L1")==0) {
// 			  printf("Insert execution time:\n");
// 			  scanf("%f",&execution);
// 			  printf("Insert idle time:\n");
// 			  scanf("%d",&idle);
// 			  l1tcm(logicalCPU,execution,idle);
// 			}
// 
// //			Caso L2 Total Cache Misses
// 
// 			if(strcmp(line,"L2")==0) {
// 			  printf("Insert execution time:\n");
// 			  scanf("%f",&execution);
// 			  printf("Insert idle time:\n");
// 			  scanf("%d",&idle);
// 			  l2tcm(logicalCPU,execution,idle);
// 			}
// 		}
// 		break;
// 	    // Rijndael benchmark
// 	    case 4: {
// 		rijnd(logicalCPU);
// 		}
// 		break;
// 	  }
// 	}
// }
