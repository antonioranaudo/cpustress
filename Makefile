PROGRAMS = cpustress
SOURCES  = rijndael-test-fst.c rijndael-api-fst.c rijndael-alg-fst.c check.c function.c

LIBS     = -lpapi -lgomp -lpthread
OBJECTS  = rijndael-test-fst.o rijndael-api-fst.o rijndael-alg-fst.o check.o function.o
CFLAGS   = -g -DTRACE_KAT_MCT -DINTERMEDIATE_VALUE_KAT -D_GNU_SOURCE
#source: rijndael-test-fst.c rijndael-api-fst.c rijndael-alg-fst.c
#objects: rijndael-test-fst.o rijndael-api-fst.o rijndael-alg-fst.o
#c flag: -DTRACE_KAT_MCT -DINTERMEDIATE_VALUE_KAT
all: $(PROGRAMS)


clean:
	rm -f $(PROGRAMS)
	rm -f $(OBJECTS)

# gcc -g main3.c -o main3 rijndael-api-fst.o rijndael-alg-fst.o -lpapi -DTRACE_KAT_MCT -DINTERMEDIATE_VALUE_KAT -pthread -lgomp  -D_GNU_SOURCE

cpustress:
	$(CC) $(SOURCES) -c $(CFLAGS)
	$(CC) $(OBJECTS) main.c -o $(PROGRAMS) $(LIBS) -static

#
# Set our CommEngine directory (by splitting the pwd into two words
# at /userspace and taking the first word only).
# Then include the common defines under CommEngine.
#
CURR_DIR :=$(shell pwd)
BUILD_DIR:=$(subst /userapp, /userapp,$(CURR_DIR))
BUILD_DIR:=$(word 1, $(BUILD_DIR))




#
# Include the rule for making dependency files.
# The '-' in front of the second include suppresses
# error messages when make cannot find the .d files.
# It will just regenerate them.
# See Section 4.14 of Gnu Make.
#


-include $(OBJS:.o=.d)
